# project-euler

I'm trying to solve all the [project euler](https://projecteuler.net/)
problems. Here are all my solution, in Clojure, with some test.

Keep in mind that, in the sources, when you see a comment about the
"Elapsed time" that is the time for the first run of the function, not
an average or whatsoever.

The answers are [here](./doc/Answers.md).

## License

Copyright © 2017 Omar Polo

Distributed under the Eclipse Public License either version 1.0 or (at
your option) any later version.
