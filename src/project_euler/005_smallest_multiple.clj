(ns project-euler.005-smallest-multiple)

;; 2520 is the smallest number that can be divided by each of the
;; numbers from 1 to 10 without any remainder.
;;
;; What is the smallest positive number that is evenly divisible
;; (i.e. divisible with no remainder) by all of the numbers from 1 to
;; 20?

(def numbers (range 20 10 -1))

(defn divisible-by?
  "`true` if and only if `num` is evenly divisible by all number in list"
  [^Integer num list]
  (every? #(= 0 (rem num %)) list))

(defn smallest-multiple
  "Calculate the smallest number that can be divided by each of the
  number in the given list (defaults to `numbers`)."
  ([]        (smallest-multiple numbers (* 20 11) 2))
  ([l]       (smallest-multiple l       1         1))
  ([l start] (smallest-multiple l       start     1))
  ([l start incr]
   (loop [x start]
     (if (divisible-by? x l)
       x
       (recur (+ incr x))))))

(comment
  (time (do (println (smallest-multiple))
            nil)))

;; => 232792560
;; => "Elapsed time: 21503.585178 msecs"
