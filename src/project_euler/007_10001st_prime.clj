(ns project-euler.007-10001st-prime
  (:require [project-euler.003-largest-prime-factor :refer [lazy-primes]]))

;; By listing the first six prime numbers: 2, 3, 5, 7, 11, and 13, we
;; can see that the 6th prime is 13.
;; 
;; What is the 10 001st prime number?

(defn nth-prime-number
  "Calculate the nth prime number."
  [n]
  (->> (lazy-primes)
       (drop (dec n))
       (take 1)
       first))


(comment
  (time
   (do (println (nth-prime-number 10001))
       nil)))

;; 104743
;; "Elapsed time: 245.862035 msecs"
