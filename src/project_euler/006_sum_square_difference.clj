(ns project-euler.006-sum-square-difference
  (:require [clojure.core.reducers :as r]
            [clojure.math.numeric-tower :refer [expt]]))

;; The sum of the squares of the first ten natural numbers is,
;; 12 + 22 + ... + 102 = 385
;;
;; The square of the sum of the first ten natural numbers is,
;; (1 + 2 + ... + 10)2 = 552 = 3025
;;
;; Hence the difference between the sum of the squares of the first
;; ten natural numbers and the square of the sum is 3025 − 385 = 2640.
;;
;; Find the difference between the sum of the squares of the first one
;; hundred natural numbers and the square of the sum.

;; `clojure.core.reducers/fold` should perform a reduction in parallel
;; on multiple cores
(defn sum-square-difference
  ([] (sum-square-difference 1 101))
  ([n m]
   (let [s (range n m)]
     (-
      (expt (r/fold + s) 2)
      (r/fold + (r/map #(expt % 2) s))))))

;; this is the "standard" "old" approach, with plain old reduce and
;; map
(defn sum-square-difference-sync
  ([] (sum-square-difference-sync 1 101))
  ([n m]
   (let [s (range n m)]
     (-
      (expt (reduce + s) 2)
      (reduce + (map #(expt % 2) s))))))


(comment
  (time
   (do (println (sum-square-difference))
       nil)))
;; 25164150
;; "Elapsed time: 0.506273 msecs"


(comment
  (time
   (do (println (sum-square-difference-sync))
       nil)))
;; 25164150
;; "Elapsed time: 0.432622 msecs"
