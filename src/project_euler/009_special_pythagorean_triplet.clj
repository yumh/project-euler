(ns project-euler.009-special-pythagorean-triplet
  (:require [clojure.math.numeric-tower :refer [expt]]))

;; A Pythagorean triplet is a set of three natural numbers, a < b < c,
;; for which, a^2 + b^2 = c^2
;;
;; For example, 32 + 42 = 9 + 16 = 25 = 52.
;;
;; There exists exactly one Pythagorean triplet for which a + b + c =
;; 1000.  Find the product abc.

(def upper-bound 1000)

(defn triplet? [a b c]
  (= (+ (expt a 2) (expt b 2)) (expt c 2)))

;; let's just bruteforce it
(defn find-triplet []
  (first
   (for [a      (range 2 upper-bound)
         b      (range 2 upper-bound)
         :let   [c (- upper-bound a b)]
         :when  (and (triplet? a b c))]
     (* a b c))))

(comment
  (time (do (println (find-triplet))
            nil)))

;; 31875000
;; "Elapsed time: 255.239942 msecs"
