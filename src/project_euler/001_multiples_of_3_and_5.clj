(ns project-euler.001-multiples-of-3-and-5)

;; If we list all the natural numbers below 10 that are multiples of 3 or 5, we get 3, 5, 6 and 9. The sum of these multiples is 23.
;;
;; Find the sum of all the multiples of 3 or 5 below 1000.

(def max-val 1000)

(defn mul-of
  "Return a lazy seq of multiple of the given number."
  [x]
  (iterate #(+ x %) x)) ; lazy seq are soooo cool

(def mul-of-3
  (mul-of 3))

(def mul-of-5
  (mul-of 5))

(defn mul-of-3-and-5-under-max
  "Sums all the multiple of 3 and 5 under an optional max value
  (default to `max-val`)."
  ([] (mul-of-3-and-5-under-max max-val))
  ([max]
   (+
    (reduce + (take-while #(> max %) mul-of-3))
    (reduce + (take-while #(> max %) mul-of-5)))))

(comment
  (mul-of-3-and-5-under-max))
;; => 266333
