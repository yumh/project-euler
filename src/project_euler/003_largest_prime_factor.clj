(ns project-euler.003-largest-prime-factor)

;; The prime factors of 13195 are 5, 7, 13 and 29.
;;
;; What is the largest prime factor of the number 600851475143 ?

(def my-big-number 600851475143)

(defn lazy-primes
  "Generate a lazy, infinite, seq of primes."
  ([] (cons 2 (lazy-primes 3 [])))
  ([n s]
   (if (not-any? #(zero? (mod n %))
                 (take-while #(<= (*' % %) n) s))
     (lazy-seq (cons n
                     (lazy-primes
                      (+' n 2)
                      (conj s n))))
     (recur (+' n 2) s))))

(defn factorize
  "Calculate the prime factor of `n`."
  [n]
  (loop [n n
         v []
         primes (lazy-primes)]
    (if (= n 1)
      v
      (let [factor (first primes)]
        (if (= 0 (mod n factor))
          (recur (quot n factor) (conj v factor) primes)
          (recur n v (rest primes)))))))

(defn largest-prime-factor
  "Calculate the largest prime factor of `n`."
  [n]
  (last (factorize n)))

(comment
  (time
   (do (println (largest-prime-factor my-big-number))
       nil)))
;; => 6857
;; => "Elapsed time: 11.732327 msecs"
