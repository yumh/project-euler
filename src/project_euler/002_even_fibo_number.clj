(ns project-euler.002-even-fibo-number)

;; Each new term in the Fibonacci sequence is generated by adding the
;; previous two terms. By starting with 1 and 2, the first 10 terms
;; will be:
;;
;; 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, ...
;;
;; By considering the terms in the Fibonacci sequence whose values do
;; not exceed four million, find the sum of the even-valued terms.

(defn fib
  ([]
   (fib 1 1))
  ([a b]
   (lazy-seq (cons a (fib b (+' a b))))))

(def max-fib 4000000)

(defn fibo-under [max]
  (take-while #(> max %) (fib)))

(defn sum-even-fibo
  "Sums all even Fibonacci number under an optional max value
  (defaults to `max-fib`."
  ([] (sum-even-fibo max-fib))
  ([max]
   (reduce +' (filter even? (fibo-under max)))))

(comment
  (sum-even-fibo))
;; => 4613732
