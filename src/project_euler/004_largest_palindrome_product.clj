(ns project-euler.004-largest-palindrome-product
  (:require [clojure.string :as s]))

;; A palindromic number reads the same both ways. The largest
;; palindrome made from the product of two 2-digit numbers is 9009 =
;; 91 × 99.
;;
;; Find the largest palindrome made from the product of two 3-digit
;; numbers.

(defn palindrome?
  "Check if the given number is palindromic"
  [^Integer n]
  (let [m (str n)]
    (= m (s/reverse m))))

(def upper-limit 999)
(def lower-limit 99)

(defn largest-palindrome
  "Find the largest palindromic number that is a product of two number
  between an upper and lower limit."
  ([] (largest-palindrome upper-limit lower-limit))
  ([upper lower]
   (reduce max
    (for [x    (range upper lower -1)
          y    (range upper lower -1)
          :let [z (* x y)]
          :when (palindrome? z)]
      z))))

(comment
  (time (do (println (largest-palindrome))
            nil)))

;; => 906609
;; => "Elapsed time: 243.863608 msecs"
