(ns project-euler.010-summation-of-primes)

;; The sum of the primes below 10 is 2 + 3 + 5 + 7 = 17.
;;
;; Find the sum of all the primes below two million.

;; directly from 3rd problem
(defn lazy-primes
  "Generate a lazy, infinite, seq of primes."
  ([] (cons 2 (lazy-primes 3 [])))
  ([n s]
   (if (not-any? #(zero? (mod n %))
                 (take-while #(<= (*' % %) n) s))
     (lazy-seq (cons n
                     (lazy-primes
                      (+' n 2)
                      (conj s n))))
     (recur (+' n 2) s))))

(defn primes-sum
  "Sum all the primes until `n`."
  [n]
  (reduce +' (take-while #(< % n) (lazy-primes))))


(comment
  (time (do (println (primes-sum 2000000))
            nil)))

;; 142913828922
;; "Elapsed time: 11217.524329 msecs"
