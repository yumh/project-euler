(ns project-euler.002-even-fibo-number-test
  (:require [clojure.test :refer :all]
            [project-euler.002-even-fibo-number :refer :all]))

(deftest sum-even-fibo-test
  (testing "The sum is correct for some small value."
    (let [s (reduce + (filter even? '(1 1 2 3 5 8 13 21 34 55 89)))]
      (is (= s (sum-even-fibo 90))))))
