(ns project-euler.007-10001st-prime-test
  (:require [clojure.test :refer :all]
            [project-euler.007-10001st-prime :refer :all]))

(deftest nth-prime-number-test
  (testing "`nth-prime-number` against given data."
    (is (= 13 (nth-prime-number 6)))))
