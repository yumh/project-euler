(ns project-euler.001-multiples-of-3-and-5-test
  (:require [clojure.test :refer :all]
            [project-euler.001-multiples-of-3-and-5 :refer :all]))

(deftest multiple-of-3-and-5-test
  (testing "correctness of `mul-of-3`"
    (is (= '(3 6 9 12) (take 4 mul-of-3))))
  (testing "correctness of `mul-of-5`"
    (is (= '(5 10 15 20 25) (take 5 mul-of-5))))
  (testing "with max val of 10"
    (is (= 23 (mul-of-3-and-5-under-max 10)))))
