(ns project-euler.004-largest-palindrome-product-test
  (:require [clojure.test :refer :all]
            [project-euler.004-largest-palindrome-product :refer :all]))

(deftest largest-palindrome-product
  (testing "the palindrome? function"
    (is (palindrome? 44))
    (is (not (palindrome? 45))))

  (testing "the largest palindromic number that is product of 2-digit integer"
    (is (= 9009 (largest-palindrome 99 9)))))
