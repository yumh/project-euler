(ns project-euler.006-sum-square-difference-test
  (:require [clojure.test :refer :all]
            [project-euler.006-sum-square-difference :refer :all]))

(deftest sum-square-difference-test
  (testing "against given data"
    (is (= 2640 (sum-square-difference 1 11)))
    (is (= 2640 (sum-square-difference-sync 1 11)))))
