(ns project-euler.005-smallest-multiple-test
  (:require [clojure.test :refer :all]
            [project-euler.005-smallest-multiple :refer :all]))

(deftest smallest-multiple-test
  (testing "the `is-divisible-by?` function"
    (is (divisible-by? 20 [2 10 5]))
    (is (not (divisible-by? 106 [2 3 7]))))

  (testing "`smallest-multiple` against given data"
    (is (= 2520 (smallest-multiple (range 1 11))))))
