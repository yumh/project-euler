(ns project-euler.003-largest-prime-factor-test
  (:require [clojure.test :refer :all]
            [project-euler.003-largest-prime-factor :refer :all]))

(deftest largest-prime-factor-test
  (testing "the correctness of the factorize function"
    (let [n  13195
          p  [5 7 13 29]
          p' (factorize n)]
      (is (= p p')))))
