(ns project-euler.009-special-pythagorean-triplet-test
  (:require [clojure.test :refer :all]
            [project-euler.009-special-pythagorean-triplet :refer :all]))

(deftest special-pythagorean-triplet-test
  (testing "the `triplet` function."
    (is (triplet? 3 4 5))))
