(defproject project-euler "0.1.0-SNAPSHOT"
  :description "The solution to the project euler problems"
  :url "https://gitlab.com/yumh/project-euler"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.8.0"]
                 [org.clojure/math.numeric-tower "0.0.4"]])
